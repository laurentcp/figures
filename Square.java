import java.awt.*;

/**
 * Un carré qui peut être manipulé et qui se dessine lui-même sur un 'canvas'.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.07.31
 */

public class Square extends Movable
{
    private int size;
    private String color;
    private boolean isVisible;

    /**
     * Crée un nouveau carré à la position par défaut
     * avec la couleur par défaut.
     */
    public Square()
    {
        size = 60;
        xPosition = 310;
        yPosition = 120;
        color = "red";
        isVisible = false;
    }

    /**
     * Rend ce carré visible. Si il était déjà visible, ne fait rien.
     */
    public void makeVisible()
    {
        isVisible = true;
        draw();
    }
    
    /**
     * Rend ce carré invisible. Si il était déjà invisible, ne fait rien.
     */
    public void makeInvisible()
    {
        erase();
        isVisible = false;
    }
    
    /**
     * Déplace le carré de quelques pixels à droite.
     */
    public void moveRight()
    {
        moveHorizontal(20);
    }

    /**
     * Déplace le carré de quelques pixels à gauche.
     */
    public void moveLeft()
    {
        moveHorizontal(-20);
    }

    /**
     * Déplace le carré de quelques pixels vers le haut.
     */
    public void moveUp()
    {
        moveVertical(-20);
    }

    /**
     * Déplace le carré de quelques pixels vers le bas.
     */
    public void moveDown()
    {
        moveVertical(20);
    }

    /**
     * Déplace le carré horizontalemnt de 'distance' pixels.
     *
     * @param distance La longueur du déplacement en pixels.
     */
    public void moveHorizontal(int distance)
    {
        erase();
        xPosition += distance;
        draw();
    }

    /**
     * Déplace le carré verticalement de 'distance' pixels.
     *
     * @param distance La longueur du déplacement en pixels.
     */
    public void moveVertical(int distance)
    {
        erase();
        yPosition += distance;
        draw();
    }

  

    /**
     * Remplace la taille actuelle du carré par la nouvelle taille.
     *
     * @param newSize La nouvelle taille en pixels. Doit être &gt;= 0.
     */
    public void changeSize(int newSize)
    {
        erase();
        size = newSize;
        draw();
    }

    /**
     * Change la couleur.
     *
     * @param newColor La nouvelle couleur, les valeurs possibles sont "red",
     * "yellow", "blue", "green", "magenta" and "black".
     */
    public void changeColor(String newColor)
    {
        color = newColor;
        draw();
    }

    /**
     * Dessine le carré à l'écran avec les caractéristiques actuelles.
     */
    protected void draw()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.draw(this, color,
                        new Rectangle(xPosition, yPosition, size, size));
            canvas.wait(10);
        }
    }

    /**
     * Retire le carré de l'écran.
     */
    private void erase()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
