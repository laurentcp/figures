import java.awt.*;
import java.awt.geom.*;

/**
 * Un cercle qui peut être manipulé et qui se dessine lui-même sur un canvas.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2011.07.31
 */

public class Circle extends Movable
{
    private int diameter;
    private String color;
    private boolean isVisible;
    
    /**
     * Crée un nouveau cercle de taille par défaut, à la position par défaut 
     * avec la couleur par défaut.
     */
    public Circle()
    {
        diameter = 68;
        xPosition = 230;
        yPosition = 90;
        color = "blue";
    }

    /**
     * Rend ce cercle visible. Si il est déjà visible, ne fait rien.
     */
    public void makeVisible()
    {
        isVisible = true;
        draw();
    }
    
    /**
     * Rend ce cercle invisible. Si il est déjà invisible, ne fait rien.
     */
    public void makeInvisible()
    {
        erase();
        isVisible = false;
    }
    
    /**
     * Déplace le cercle de quelques pixels à droite.
     */
    public void moveRight()
    {
        moveHorizontal(20);
    }

    /**
     * Déplace le cercle de quelques pixels à gauche.
     */
    public void moveLeft()
    {
        moveHorizontal(-20);
    }

    /**
     * Déplacer le cercle de quelques pixels vers le haut.
     */
    public void moveUp()
    {
        moveVertical(-20);
    }

    /**
     * Déplacer le cercle de quelques pixels vers le bas.
     */
    public void moveDown()
    {
        moveVertical(20);
    }

    /**
     * Déplace le cercle horizontalement de 'distance' pixels.
     * 
     * @param distance La longueur du déplacement en pixels.
     */
    public void moveHorizontal(int distance)
    {
        erase();
        xPosition += distance;
        draw();
    }

    /**
     * Déplace le cercle verticalement de 'distance' pixels.
     * 
     * @param distance La longueur du déplacement en pixels.
     */
    public void moveVertical(int distance)
    {
        erase();
        yPosition += distance;
        draw();
    }

  

    /**
     * Change la taille vers la nouvelle taille (en pixels). 
     * 
     * @param newDiameter Le nouveau diamètre en pixels, il doit être >= 0.
     */
    public void changeSize(int newDiameter)
    {
        erase();
        diameter = newDiameter;
        draw();
    }

    /**
     * Change la couleur.
     * 
     * @param newColor La nouvelle couleur, les valeurs possibles sont "red", 
     * "yellow", "blue", "green", "magenta" and "black".
     */
    public void changeColor(String newColor)
    {
        color = newColor;
        draw();
    }

    /**
     * Dessine le cercle à l'écran avec les spécifications courantes.
     */
    protected void draw()
    {
        if (isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.draw(this, color, new Ellipse2D.Double(xPosition, yPosition, 
                                                          diameter, diameter));
            canvas.wait(10);
        }
    }

    /**
     * Efface le cercle de l'écran.
     */
    private void erase()
    {
        if (isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
