
/**
 * D�crivez votre classe abstraite Movable ici.
 *
 * @author  (votre nom)
 * @version (un num�ro de version ou une date)
 */
public abstract class Movable
{
    // variable d'instance - remplacez cet exemple par le v�tre
    protected int xPosition;
    protected int yPosition;

 /**
     * Déplace horizontalement et lentement le triangle  de 'distance' pixels.
     *
     * @param distance La longueur du déplacement en pixels.
     */
    public void slowMoveHorizontal(int distance)
    {
        int delta;
        int steps = distance;

        if(distance < 0) 
        {
            delta = -1;
            steps = -distance;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < steps; i++)
        {
            xPosition += delta;
            draw();
        }
    }

    /**
     * Déplace horizontalement et lentement le triangle  de 'distance' pixels.
     *
     * @param distance La longueur du déplacement en pixels.
     */
    public void slowMoveVertical(int distance)
    {
        int delta;
        int steps = distance;

        if(distance < 0) 
        {
            delta = -1;
            steps = -distance;
        }
        else 
        {
            delta = 1;
        }

        for(int i = 0; i < steps; i++)
        {
            yPosition += delta;
            draw();
        }
    } 
    
    /**
     * Dessine le triangle à l'écran avec les caractéristiques actuelles.
     */
    abstract void draw();
}
